const text = await Deno.readTextFile("./input.txt");

let valves = text.split("\n")
  .map(line => /Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? ([A-Z, ]+)/.exec(line))
  .map(match => {
    return {
      id: match[1],
      rate: match[2] * 1,
      nodes: match[3].split(", ")
    }
  });

let valveObj = {};
valves.forEach(v => valveObj[v.id] = v);

function getWeights(valves) {
  let valvesThatMatter = valves.filter(v => v.id == "AA" || v.rate > 0);
  let weights = {};
  valvesThatMatter.forEach(v => {
    weights[v.id] = {};
    valvesThatMatter.forEach(v2 => {
      weights[v.id][v2.id] = dijkstra(v.id, v2.id, valves);
    });
  });
  return weights;
}

function findLowest(vertices) {
  return Object.entries(vertices).reduce((a, b) => a[1].dist < b[1].dist ? a : b)[0];
}

function dijkstra(start, end, valves) {
  let vertices = {};

  valves.forEach(valve => {
    vertices[valve.id] = {
      dist: valve.id == start ? 0 : Infinity,
      prev: null,
      next: valve.nodes
    }
  })

  while (Object.entries(vertices).length > 0) {
    let vertCoord = findLowest(vertices);
    let vert = vertices[vertCoord];
    delete vertices[vertCoord];

    if (vertCoord == end) {
      return vert.dist;
    }

    vert.next.forEach(nextId => {
      var next = vertices[nextId];
      if (next && vert.dist + 1 < next.dist) {
        next.dist = vert.dist + 1;
        next.prev = vertCoord;
      }
    })
  }

  throw "We didn't reach the end";

}

let count = 0;

function bruteForce(weights, visited, start, time, allowed = null) {
  count++;
  let pos = start;
  let visited2 = new Set(visited);
  visited2.add(pos);

  let nexts = Object.keys(weights[start])
    .filter(w => allowed == null || allowed.includes(w))
    .filter(w => !visited2.has(w)) // we haven't already visited it
    .filter(w => weights[start][w] + 1 < time) // there is enough time for it to matter
    .sort((a, b) => weights[start][a] - weights[start][b]);
    
  let max = 0;
  
  nexts.forEach((n, i) => {
    let remainingTime = time - weights[start][n] - 1;
    let thisVolume = remainingTime * valveObj[n].rate;
    
    // code works without this, but runs slower
    let bestRest = nexts.filter((n2, i2) => i2 != i).map((n2, i2) => {
      let remainingTime2 = remainingTime - (2 * (i2 + 1));
      return Math.max(remainingTime2, 0) * valveObj[n2].rate;
    }).reduce((a,b) => a+b, 0);
    
    if (max >= thisVolume + bestRest) {
      return; // short circuit
    }
    
    let rest = bruteForce(weights, visited2, n, remainingTime, allowed);
    max = Math.max(thisVolume + rest, max);
  })
  
  return max;
}

console.log(bruteForce(getWeights(valves), new Set(), "AA", 30));

// This solution basically just tries all possible splits of
// valves between you and the elephant. This increases exponentially
// as the number of valves increases, but works well enough for
// this problem.
function part2() {
  let valvesThatMatter = valves.filter(v => v.rate > 0);
  let max = 0;
  let weights = getWeights(valves);
  
  for (let i = 0; i < Math.pow(2, valvesThatMatter.length); i++) {
    let split = i.toString(2).padStart(valvesThatMatter.length, '0').split("");
    let human = [];
    let elephant = [];
    for (let n=0; n<split.length; n++) {
      let valve = valvesThatMatter[n];
      if (split[n] == '1') {
        elephant.push(valve.id);
      } else {
        human.push(valve.id);
      }
    }
    
    if (elephant.length > human.length) {
      // since this problem is symetric, we can add a small optimization
      continue;
    }
        
    let score = bruteForce(weights, new Set(), "AA", 26, elephant) + bruteForce(weights, new Set(), "AA", 26, human);
    max = Math.max(max, score)
  }
  
  return max;
}

console.log(part2());
