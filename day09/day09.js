const text = await Deno.readTextFile("./input.txt");

const instructions = text.split("\n")
  .map(line => /([A-Z]) (\d+)/.exec(line))
  .map(match => {
    return {
      direction: match[1],
      distance: match[2]
    }
  });

function countTailSpots(instructions, ropeLength) {
  let tailSpots = new Set();

  // create starting rope
  let rope = [...Array(ropeLength).keys()].map(it => {
    return {
      x: 0,
      y: 0
    }
  });

  for (let i = 0; i < instructions.length; i++) {
    let instruction = instructions[i];
    for (let s = 0; s < instruction.distance; s++) {
      // move head
      if (instruction.direction == "U") {
        rope[0].y++;
      } else if (instruction.direction == "D") {
        rope[0].y--;
      } else if (instruction.direction == "R") {
        rope[0].x++;
      } else if (instruction.direction == "L") {
        rope[0].x--;
      } else {
        throw `Invalid direction '${instruction.direction}'`
      }

      for (let k = 1; k < rope.length; k++) {
        let headX = rope[k - 1].x;
        let headY = rope[k - 1].y;
        let tailX = rope[k].x;
        let tailY = rope[k].y;
        // move tail
        if (Math.abs(headX - tailX) <= 1 && Math.abs(headY - tailY) <= 1) {
          // do nothing
        } else if (headX == tailX) {
          // move up/down
          rope[k].y += (headY - tailY) / Math.abs(headY - tailY);
        } else if (headY == tailY) {
          // move left/right
          rope[k].x += (headX - tailX) / Math.abs(headX - tailX);
        } else {
          // move diagonally
          rope[k].y += (headY - tailY) / Math.abs(headY - tailY);
          rope[k].x += (headX - tailX) / Math.abs(headX - tailX);
        }
        
        // only add if this is the end of the rope
        if (k == ropeLength - 1) {
          tailSpots.add(`x:${rope[k].x},y:${rope[k].y}`);
        }
      }
    }
  }
  return tailSpots.size;
}

// part 1
console.log(countTailSpots(instructions, 2))

//part 2
console.log(countTailSpots(instructions, 10))


