// part 1

const text = await Deno.readTextFile("./input.txt");

function lineToRanges(line) {
  let elves = line.split(",");
  return [
    {
      start: elves[0].split("-")[0] * 1,
      end: elves[0].split("-")[1] * 1
    },
    {
      start: elves[1].split("-")[0] * 1,
      end: elves[1].split("-")[1] * 1
    }
  ];
}


function contains(a, b) {
  if (a.start <= b.start && a.end >= b.end) {
    return true;
  }
  
  return false;
}

function overlaps(a, b) {
  if (a.start >= b.start && a.start <= b.end) {
    return true;
  }

  if (a.end >= b.start && a.end <= b.end) {
    return true;
  }
  
  if (b.start >= a.start && b.start <= a.end) {
    return true;
  }

  if (b.end >= a.start && b.end <= a.end) {
    return true;
  }
  
  return false;
}

const out = text.split("\n").map(lineToRanges).filter(ranges => contains(ranges[0], ranges[1]) || contains(ranges[1], ranges[0])).length;
console.log(out);

const out2 = text.split("\n").map(lineToRanges).filter(ranges => overlaps(ranges[0], ranges[1])).length;
console.log(out2);