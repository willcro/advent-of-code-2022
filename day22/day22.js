const text = await Deno.readTextFile("./input.txt");

const mapText = text.split("\n\n")[0];
const instructionsText = text.split("\n\n")[1];
const width = mapText.split("\n").map(it => it.length).reduce((a, b) => Math.max(a, b));
let map = mapText.split("\n").map(it => it.padEnd(width + 1, " ").padStart(width + 2, " ")).map(it => it.split(""));

// add padding all around so I don't have to worry about the edge of the map
map = ["".padEnd(width + 2, " ").split("")].concat(map).concat(["".padEnd(width + 2, " ").split("")]);
const instructions = instructionsText.split(/(\d+|[A-Z])/).filter(it => it != "");

const circle = [[1, 0], [0, 1], [-1, 0], [0, -1]];

function part2(map, instructions) {
  let x = 1;
  let y = 1;
  let facing = 0;

  // move to start
  while (map[y][x] != ".") {
    x++;
  }

  // execute instructions
  instructions.forEach(instruction => {
    if (/\d+/.test(instruction)) {
      let amount = instruction * 1;
      for (var i = 0; i < amount; i++) {
        let nextSpot = nextPart2(map, x, y, facing);
        x = nextSpot[0];
        y = nextSpot[1];
        facing = nextSpot[2];
        // console.log(`x: ${x}, y ${y}`);
      }

    } else if (instruction == "L") {
      facing = ((facing - 1) + 4) % 4;
    } else if (instruction == "R") {
      facing = (facing + 1) % 4;
    } else {
      throw `Unknown instruction ${instruction}`;
    }
  })

  // return [x,y,facing];
  return 1000 * y + 4 * x + facing;
}

function next(map, x, y, facing) {
  let dir = circle[facing];
  let nextX = x + dir[0];
  let nextY = y + dir[1];

  if (map[nextY][nextX] == ".") {
    return [nextX, nextY];
  }

  if (map[nextY][nextX] == "#") {
    return [x, y];
  }

  nextX = nextX - dir[0];
  nextY = nextY - dir[1];

  while (map[nextY][nextX] != " ") {
    nextX = nextX - dir[0];
    nextY = nextY - dir[1];
  }

  nextX = nextX + dir[0];
  nextY = nextY + dir[1];

  if (map[nextY][nextX] == "#") {
    return [x, y];
  }

  return [nextX, nextY];
}

function nextPart2(map, x, y, facing) {
  let dir = circle[facing];
  let nextX = x + dir[0];
  let nextY = y + dir[1];
  let nextFacing = facing;

  if (map[nextY][nextX] == ".") {
    return [nextX, nextY, nextFacing];
  }

  if (map[nextY][nextX] == "#") {
    return [x, y, nextFacing];
  }
  
  // 1a
  if (nextY == 0 && nextX >= 51 && nextX <= 100 && facing == 3) {
    console.log("Crossing border 1a");
    nextX = 1;
    nextY = x + 100;
    nextFacing = 0;
  }
  
  // 1b
  else if (nextX == 0 && nextY >= 151 && nextY <= 200 && facing == 2) {
    console.log("Crossing border 1b");
    nextX = y - 100;
    nextY = 1;
    nextFacing = 1;
  }
  
  // 2a
  else if (nextY == 201 && nextX >= 1 && nextX <= 50 && facing == 1) {
    console.log("Crossing border 2a");
    nextX = x + 100;
    nextY = 1;
    nextFacing = 1;
  }
  
  // 2b
  else if (nextY == 0 && nextX >= 101 && nextX <= 150 && facing == 3) {
    console.log("Crossing border 2b");
    nextX = x - 100;
    nextY = 200;
    nextFacing = 3;
  }
  
  // 3a
  else if (nextX == 51 && nextY >= 151 && nextY <= 200 && facing == 0) {
    console.log("Crossing border 3a");
    nextX = y - 100;
    nextY = 150;
    nextFacing = 3;
  }
  
  // 3b
  else if (nextY == 151 && nextX >= 51 && nextX <= 100 && facing == 1) {
    console.log("Crossing border 3b");
    nextX = 50;
    nextY = x + 100;
    nextFacing = 2;
  }
  
  // 4a
  else if (nextX == 0 && nextY >= 101 && nextY <= 150 && facing == 2) {
    console.log("Crossing border 4a");
    nextX = 51;
    nextY = 151-y;
    nextFacing = 0;
  }
  
  // 4b
  else if (nextX == 50 && nextY >= 1 && nextY <= 50 && facing == 2) {
    console.log("Crossing border 4b");
    nextX = 1;
    nextY = 151 - y;
    nextFacing = 0;
  }
  
  // 5a
  else if (nextX == 50 && nextY >= 51 && nextY <= 100 && facing == 2) {
    console.log("Crossing border 5a");
    nextX = y - 50;
    nextY = 101;
    nextFacing = 1;
  }
  
  // 5b
  else if (nextY == 100 && nextX >= 1 && nextX <= 50 && facing == 3) {
    console.log("Crossing border 5b");
    nextX = 51;
    nextY = x + 50;
    nextFacing = 0;
  }
  
  // 6a
  else if (nextX == 101 && nextY >= 51 && nextY <= 100 && facing == 0) {
    console.log("Crossing border 6a");
    nextX = y + 50;
    nextY = 50;
    nextFacing = 3;
  }
  
  // 6b
  else if (nextY == 51 && nextX >= 101 && nextX <= 150 && facing == 1) {
    console.log("Crossing border 6b");
    nextX = 100;
    nextY = x - 50;
    nextFacing = 2;
  }
  
  // 7a
  else if (nextX == 101 && nextY >= 101 && nextY <= 150 && facing == 0) {
    console.log("Crossing border 7a");
    nextX = 150;
    nextY = 151 - y;
    nextFacing = 2;
  }
  
  // 7b
  else if (nextX == 151 && nextY >= 1 && nextY <= 50 && facing == 0) {
    console.log("Crossing border 7b");
    nextX = 100;
    nextY = 151 - y;
    nextFacing = 2;
  } else {
    throw `x: ${nextX}, y: ${nextY}, facing: ${facing}`
  }
  
  if (map[nextY][nextX] == "#") {
    return [x, y, facing];
  }

  return [nextX, nextY, nextFacing];
}

console.log(part2(map, instructions))

// tests
function runTests() {
  console.log(nextPart2(map, 51, 1, 3).toString() == [1,151,0].toString());
  console.log(nextPart2(map, 1, 151, 2).toString() == [51, 1, 1].toString());
  
  console.log(nextPart2(map, 1, 200, 1).toString() == [101, 1, 1].toString());
  console.log(nextPart2(map, 101, 1, 3).toString() == [1, 200, 3].toString());
  
  console.log(nextPart2(map, 50, 200, 0).toString() == [100, 150, 3].toString());
  console.log(nextPart2(map, 100, 150, 1).toString() == [50, 200, 2].toString());
  
  console.log(nextPart2(map, 1, 150, 2).toString() == [51, 1, 0].toString());
  console.log(nextPart2(map, 51, 1, 2).toString() == [1, 150, 0].toString());
  
  console.log(nextPart2(map, 51, 51, 2).toString() == [1, 101, 1].toString());
  console.log(nextPart2(map, 2, 101, 3).toString() == [51, 52, 0].toString());
  
  console.log(nextPart2(map, 100, 51, 0).toString() == [101, 50, 3].toString());
  console.log(nextPart2(map, 101, 50, 1).toString() == [100, 51, 2].toString());
  
  console.log(nextPart2(map, 100, 101, 0).toString() == [150, 50, 2].toString());
  console.log(nextPart2(map, 150, 50, 0).toString() == [100, 101, 2].toString());
}


