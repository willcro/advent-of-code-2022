// part 1

const LOSS = 0;
const DRAW = 3;
const WIN = 6;

const letterToPlay = {
  "A": {
    play: "rock",
    points: 1,
    beats: "scissors",
    playToDraw: "A",
    playToWin: "B",
    playToLose: "C"
  },
  "B": {
    play: "paper",
    points: 2,
    beats: "rock",
    playToDraw: "B",
    playToWin: "C",
    playToLose: "A"
  },
  "C": {
    play: "scissors",
    points: 3,
    beats: "paper",
    playToDraw: "C",
    playToWin: "A",
    playToLose: "B"
  },
  "X": {
    play: "rock",
    points: 1,
    beats: "scissors"
  },
  "Y": {
    play: "paper",
    points: 2,
    beats: "rock"
  },
  "Z": {
    play: "scissors",
    points: 3,
    beats: "paper"
  }
}

const text = await Deno.readTextFile("./input.txt");

function play(opponent, mine) {
  if (opponent.play == mine.play) {
    return mine.points + DRAW;
  }

  if (opponent.beats == mine.play) {
    return mine.points + LOSS;
  }

  return mine.points + WIN;

}

var games = text.split("\n")
    .map(it => it.split(" "))
    .map(it => {
      return {
        opponent: letterToPlay[it[0]],
        mine: letterToPlay[it[1]]
      }
    })
    .map(game => play(game.opponent, game.mine))
    .reduce((a,b) => a + b, 0)
    

console.log(games);

function part2(opponentPlay, letter) {
  if (letter == "X") {
    return letterToPlay[opponentPlay.playToLose];
  } else if (letter == "Y") {
    return letterToPlay[opponentPlay.playToDraw];
  } else {
    return letterToPlay[opponentPlay.playToWin];
  }
}

var games2 = text.split("\n")
  .map(it => it.split(" "))
  .map(it => {
    return {
      opponent: letterToPlay[it[0]],
      mine: part2(letterToPlay[it[0]], it[1])
    }
  })
  .map(game => play(game.opponent, game.mine))
  .reduce((a, b) => a + b, 0);

console.log(games2)