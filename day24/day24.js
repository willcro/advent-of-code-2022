const text = await Deno.readTextFile("./input.txt");

// note: there are no up/down blizzards in the far left or right columns
// this simplifies things a lot.

function getBlizzards() {
  return text.split("\n").flatMap((line, y) => line.split("").map((c, x) => {
    if (c == "<") {
      return {
        dir: [-1, 0],
        x: x,
        y: y
      }
    } else if (c == ">") {
      return {
        dir: [1, 0],
        x: x,
        y: y
      }
    } else if (c == "^") {
      return {
        dir: [0, -1],
        x: x,
        y: y
      }
    } else if (c == "v") {
      return {
        dir: [0, 1],
        x: x,
        y: y
      }
    }

    return null;
  })).filter(it => it != null);
}

let blizzards = getBlizzards();

let width = text.split("\n")[0].length - 2;
let height = text.split("\n").length - 2;


function getNodes(startX, startY, goalX, goalY) {
  // let out = new Set();
  let out = {};
  let uniquePositions = width * height;
  for (let t = 0; t < uniquePositions; t++) {
    let blizzardLocations = new Set(blizzards.map(b => `${b.x},${b.y}`));
    // console.log(blizzards);

    for (let y = 1; y <= height; y++) {
      for (let x = 1; x <= width; x++) {
        if (!blizzardLocations.has(`${x},${y}`)) {
          out[`${x},${y},${t}`] = {
            id: `${x},${y},${t}`,
            x: x,
            y: y,
            t: t,
            heuristic: (width - x) + (height + 1 - y),
            distance: Infinity
          };
        }
      }
    }

    out[`1,0,${t}`] = {
      id: `1,0,${t}`,
      x: 1,
      y: 0,
      t: t,
      heuristic: (width - 1) + (height + 1 - 0),
      distance: Infinity
    }; // add start

    out[`${width},${height + 1},${t}`] = {
      id: `${width},${height + 1},${t}`,
      x: width,
      y: height + 1,
      t: t,
      heuristic: 0,
      distance: Infinity
    }; // add end

    blizzards = blizzards.map(b => {
      return {
        dir: b.dir,
        x: ((b.x + b.dir[0] + (width - 1)) % width) + 1,
        y: ((b.y + b.dir[1] + (height - 1)) % height) + 1,
      }
    })
  }

  return out;
}

function findLowest(vertices) {
  return Object.entries(vertices).reduce((a, b) => (a[1].distance) < (b[1].distance) ? a : b)[0];
}

function dijkstra(vertices, goalX, goalY, start) {

  let toSearch = {};
  toSearch[start] = vertices[start]

  while (Object.entries(toSearch).length > 0) {
    let vertCoord = findLowest(toSearch);
    let vert = toSearch[vertCoord];
    delete toSearch[vertCoord];
    delete vertices[vertCoord];

    if (vert.x == goalX && vert.y == goalY) {
      return vert.distance;
    }

    [
      [0, -1], // up
      [0, 1],  // down
      [-1, 0], // left
      [1, 0],   // right
      [0, 0]
    ].forEach(transform => {
      var next = vertices[`${vert.x + transform[0]},${vert.y + transform[1]},${(vert.t + 1) % (width * height)}`];
      if (next && vert.distance + 1 < next.distance) {
        next.distance = vert.distance + 1;
        toSearch[next.id] = next;
      }
    });
  }

  throw "We didn't reach the end";
}

function part1() {
  blizzards = getBlizzards();
  let nodes = getNodes();
  nodes[`1,0,0`] = {
    id: `1,0,0`,
    x: 1,
    y: 0,
    t: 0,
    heuristic: (width - 1) + (height + 1 - 0),
    distance: 0
  }; // add start
  return dijkstra(nodes, width, height + 1, `1,0,0`);
}

// console.log(part1())

function part2() {
  blizzards = getBlizzards();
  let nodes = getNodes();
  nodes[`1,0,0`] = {
    id: `1,0,0`,
    x: 1,
    y: 0,
    t: 0,
    heuristic: (width - 1) + (height + 1 - 0),
    distance: 0
  }; // add start

  let time = dijkstra(nodes, width, height + 1, `1,0,0`);

  console.log(time);

  blizzards = getBlizzards();
  nodes = getNodes();
  nodes[`${width},${height + 1},${time}`] = {
    id: `${width},${height + 1},${time}`,
    x: width,
    y: height + 1,
    t: time,
    heuristic: (width - 1) + (height + 1 - 0),
    distance: 0
  }; // add start

  let time2 = dijkstra(nodes, 1, 0, `${width},${height + 1},${time}`);

  console.log(time2);

  blizzards = getBlizzards();
  nodes = getNodes();
  nodes[`1,0,${(time + time2) % (width * height)}`] = {
    id: `1,0,${(time + time2) % (width * height)}`,
    x: 1,
    y: 0,
    t: (time + time2) % (width * height),
    heuristic: (width - 1) + (height + 1 - 0),
    distance: 0
  }; // add start

  let time3 = dijkstra(nodes, width, height + 1, `1,0,${(time + time2) % (width * height)}`);


  console.log(time3);

  return time + time2 + time3
}

console.log(part2())
