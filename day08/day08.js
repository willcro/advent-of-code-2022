const text = await Deno.readTextFile("./input.txt");

const grid = text.split("\n").map(line => line.split(""));

const height = grid.length;
const width = grid[0].length;

function countTrees(grid) {
  let trees = new Set();
  
  // top
  for(let x=0; x<width; x++) {
    let tallest = -1;
    let y = 0;
    while (tallest < 9 && y<height) {
      const tree = grid[y][x];
      if (tree > tallest) {
        trees.add(`${x}:${y}`);
        tallest = tree;
      }
      y++;
    }
  }
  
  // right
  for (let y = height - 1; y >= 0; y--) {
    let tallest = -1;
    let x = width - 1;
    while (tallest < 9 && x >= 0) {
      const tree = grid[y][x];
      if (tree > tallest) {
        trees.add(`${x}:${y}`);
        tallest = tree;
      }
      x--;
    }
  }
  
  // bottom
  for (let x = width-1; x >= 0; x--) {
    let tallest = -1;
    let y = height - 1;
    while (tallest < 9 && y >= 0) {
      const tree = grid[y][x];
      if (tree > tallest) {
        trees.add(`${x}:${y}`);
        tallest = tree;
      }
      y--;
    }
  }
  
  // left
  for (let y = 0; y < height; y++) {
    let tallest = -1;
    let x = 0;
    while (tallest < 9 && x < width) {
      const tree = grid[y][x];
      if (tree > tallest) {
        trees.add(`${x}:${y}`);
        tallest = tree;
      }
      x++;
    }
  }
  
  return trees.size;
}

function findScenic(grid) {
  let max = 0;
  for(let y=0; y<height; y++) {
    for(let x=0; x<width; x++) {
      let score = scenicScore(grid, x, y);
      max = Math.max(score, max);
    }
  }
  return max;
}

function scenicScore(grid, x, y) {
  const thisTree = grid[y][x];
  
  // look up
  let up = 0;
  while (y - up > 0) {
    up++;
    if (grid[y - up][x] >= thisTree) {
      break;
    }
  }
  
  // look right
  let right = 0;
  while (x + right < width - 1) {
    right++;
    if (grid[y][x + right] >= thisTree) {
      break;
    }
  } 

  // look down
  let down = 0;
  while (y + down < height - 1) {
    down++;
    if (grid[y + down][x] >= thisTree) {
      break;
    }
  } 

  // look left
  let left = 0;
  while (x - left > 0) {
    left++;
    if (grid[y][x - left] >= thisTree) {
      break;
    }
  }
  
  // console.log(`up: ${up}, down: ${down}, left: ${left}, right: ${right}`)
  return up * down * left * right;
}

// part 1
console.log(countTrees(grid))

//part 2
console.log(findScenic(grid))


