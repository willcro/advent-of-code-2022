const text = await Deno.readTextFile("./input.txt");

const elves = text.split("\n").flatMap((line, y) => line.split("").map((c, x) => {
  if (c == "#") {
    return {
      id: `${x},${y}`,
      x: x,
      y: y
    }
  } else {
    return null;
  }
})).filter(it => it != null);

function elfToString(elf) {
  return `${elf.x},${elf.y}`
}

let lookOrder = [
  {
    id: "north",
    check: [
      [0, -1],
      [-1, -1],
      [1, -1]
    ],
    move: [0, -1]
  },
  {
    id: "south",
    check: [
      [0, 1],
      [-1, 1],
      [1, 1]
    ],
    move: [0, 1]
  },
  {
    id: "west",
    check: [
      [-1, 0],
      [-1, -1],
      [-1, 1]
    ],
    move: [-1, 0]
  },
  {
    id: "east",
    check: [
      [1, 0],
      [1, -1],
      [1, 1]
    ],
    move: [1, 0]
  }
];

let allDirs = [
  [1, 0], [1, 1], [1, -1], [-1, 0], [-1, 1], [-1, -1], [0, 1], [0, -1]
]


function move(elves) {
  let elfSet = new Set(elves.map(elfToString));
  
  let proposedMoves = {};
  let proposedMovesCount = {};

  for (let i = 0; i < elves.length; i++) {
    let elf = elves[i];
    if (allDirs.every(dir => !elfSet.has(`${elf.x + dir[0]},${elf.y + dir[1]}`))) {
      proposedMoves[elf.id] = [elf.x, elf.y];
      if (proposedMovesCount[elfToString(elf)]) {
        proposedMovesCount[elfToString(elf)]++;
      } else {
        proposedMovesCount[elfToString(elf)] = 1;
      }
      continue;
    }
    
    let validLooks = lookOrder.filter(look => {
      return !look.check.some(dir => elfSet.has(`${elf.x + dir[0]},${elf.y + dir[1]}`))
    });
    
    if (validLooks.length == 0) {
      proposedMoves[elf.id] = [elf.x, elf.y];
      if (proposedMovesCount[elfToString(elf)]) {
        proposedMovesCount[elfToString(elf)]++;
      } else {
        proposedMovesCount[elfToString(elf)] = 1;
      }
      continue;
    }
    
    let look = validLooks[0];
    
    proposedMoves[elf.id] = [elf.x + look.move[0], elf.y + look.move[1]];
    let moveStr = `${elf.x + look.move[0]},${elf.y + look.move[1]}`
    if (proposedMovesCount[moveStr]) {
      proposedMovesCount[moveStr]++;
    } else {
      proposedMovesCount[moveStr] = 1;
    }  
  }
  
  for (let i = 0; i < elves.length; i++) {
    let elf = elves[i];
    let proposedMove = proposedMoves[elf.id];
    if (proposedMovesCount[`${proposedMove[0]},${proposedMove[1]}`] == 1) {
      elf.x = proposedMove[0];
      elf.y = proposedMove[1];
    } 
  }
}

function part1(elves, steps) {
  for (let i=0; i<steps; i++) {
    move(elves);
    lookOrder = lookOrder.slice(1).concat([lookOrder[0]]);
  }
  
  let xMax = elves.map(e => e.x).reduce((a,b) => Math.max(a,b));
  let xMin = elves.map(e => e.x).reduce((a, b) => Math.min(a, b));
  let yMax = elves.map(e => e.y).reduce((a, b) => Math.max(a, b));
  let yMin = elves.map(e => e.y).reduce((a, b) => Math.min(a, b));
    
  return ((xMax - xMin + 1) * (yMax - yMin + 1)) - elves.length;
}

function part2(elves) {
  let last = JSON.stringify(elves);
  for (let i = 0; i < 10000; i++) {
    move(elves);
    lookOrder = lookOrder.slice(1).concat([lookOrder[0]]);
    let now = JSON.stringify(elves);
    if (now == last) {
      return i + 1;
    }
    
    last = now;
  }
  return 10000;
}

function range(size, startAt = 0) {
  return [...Array(size).keys()].map(i => i + startAt);
}

function printElves() {
  let elfSet = new Set(elves.map(elfToString));
  let xMax = elves.map(e => e.x).reduce((a, b) => Math.max(a, b));
  let xMin = elves.map(e => e.x).reduce((a, b) => Math.min(a, b));
  let yMax = elves.map(e => e.y).reduce((a, b) => Math.max(a, b));
  let yMin = elves.map(e => e.y).reduce((a, b) => Math.min(a, b));
  
  console.log(range((yMax - yMin + 1), yMin).map(y => range((xMax - xMin + 1), xMin).map(x => elfSet.has(`${x},${y}`) ? '#' : '.').join("")).join("\n"));
}


// console.log(part1(elves, 200));

console.log(part2(elves));

printElves()

