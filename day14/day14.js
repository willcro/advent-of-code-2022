const text = await Deno.readTextFile("./input.txt");

const paths = text.split("\n").map(line => line.split(" -> ").map(pair => pair.split(",")));
const xmax = paths.flat().map(pair => pair[0]).reduce((a,b) => Math.max(a,b));
const ymax = paths.flat().map(pair => pair[1]).reduce((a, b) => Math.max(a, b));

const rocks = [...Array(ymax + 3).keys()].map((it, y) => [...Array(xmax + 200).keys()].map(it => y == (ymax + 2) ? 1 : 0))

function range(size, startAt = 0) {
  return [...Array(size).keys()].map(i => i + startAt);
}

for (let i=0; i<paths.length; i++) {
  let path = paths[i];
  for (let n=1; n<path.length; n++) {
    let fromPair = path[n-1];
    let toPair = path[n];
    let fromY = Math.min(fromPair[1], toPair[1]);
    let toY = Math.max(fromPair[1], toPair[1]);
    let fromX = Math.min(fromPair[0], toPair[0]);
    let toX = Math.max(fromPair[0], toPair[0]);
    
    for (let y=fromY; y<=toY; y++) {
      for (let x=fromX; x<=toX; x++) {
        rocks[y][x] = 1;
      }
    }
  }
}

function dropSand(n) {
  let i = 0;
  while (n > 0) {
    let x = 500;
    let y = 0;
    let done = false;
    while (!done) {
      if (y >= ymax + 2) {
        return i;
      }
      
      if (rocks[y+1][x] == 0) {
        y++;
      } else if (rocks[y+1][x-1] == 0) {
        x--;
        y++;
      } else if (rocks[y+1][x+1] == 0) {
        x++;
        y++;
      } else {
        if (x == 500 && y == 0) {
          return i + 1;
        }
        rocks[y][x] = 2;
        done = true;
      }
    }
    n--;
    i++;
  }
}

console.log(dropSand(100000));

function print() {
  Deno.writeTextFile("out.txt",  rocks.map(row => row.join("")).join("\n"));
}

print()

// console.log(rocks)
