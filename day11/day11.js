const text = await Deno.readTextFile("./input.txt");

const MONKEY_REGEX = /Monkey (\d+):\n  Starting items: (.*)\n  Operation: new = (.*)\n  Test: divisible by (\d+)\n    If true: throw to monkey (\d+)\n    If false: throw to monkey (\d+)/

// I used BigInts everywhere as a first try
// It probably would work without that but whatever
const monkeys = text.split("\n\n")
  .map(monkTxt => MONKEY_REGEX.exec(monkTxt))
  .map(match => {
    return {
      id: match[1] * 1,
      items: match[2].split(", ").map(it => BigInt(it)),
      operation: match[3].replaceAll(/(\d+)/g, "$1n"),
      divisor: BigInt(match[4] * 1),
      ifTrue: match[5] * 1,
      ifFalse: match[6] * 1,
      inspections: 0
    }
  });

function run(monkeys, rounds, divisor, top) {
  let monkDivisor = monkeys.map(m => m.divisor).reduce((a,b) => a*b, 1n);
  
  for (let r = 0; r < rounds; r++) {
    for (let m = 0; m < monkeys.length; m++) {
      let monkey = monkeys[m];
      for (let i = 0; i < monkey.items.length; i++) {
        let old = monkey.items[i];
        let after = eval(monkey.operation);
        after = after / divisor;
        after = after % monkDivisor;
        let test = after % monkey.divisor == 0;
        let newMonkey = test ? monkey.ifTrue : monkey.ifFalse;
        monkeys[newMonkey].items.push(after);
        monkey.inspections++;
      }
      monkey.items = [];
    }
  }

  return monkeys.map(m => m.inspections)
    .sort((a, b) => b - a)
    .slice(0, top)
    .reduce((a, b) => a * b, 1);
}

// run() has side effects, so you can only run once

// part 1
// console.log(run(monkeys, 20, 3n, 2));

// part2
console.log(run(monkeys, 10000, 1n, 2));
