const text = await Deno.readTextFile("./input2.txt");

const REGEX = /Blueprint (\d+): Each ore robot costs (\d+) ore\. Each clay robot costs (\d+) ore\. Each obsidian robot costs (\d+) ore and (\d+) clay\. Each geode robot costs (\d+) ore and (\d+) obsidian\./

const blueprints = text.split("\n").map(line => REGEX.exec(line)).map(match => {
  return {
    id: match[1] * 1,
    oreRobotCost: {
      ore: match[2] * 1,
      clay: 0,
      obsidian: 0
    },
    clayRobotCost: {
      ore: match[3] * 1,
      clay: 0,
      obsidian: 0
    },
    obsidianRobotCost: {
      ore: match[4] * 1,
      clay: match[5] * 1,
      obsidian: 0
    },
    geodeRobotCost: {
      ore: match[6] * 1,
      clay: 0,
      obsidian: match[7] * 1
    },
    maxOreRobots: Math.max(match[2] * 1, match[3] * 1, match[4] * 1, match[6] * 1),
    maxClayRobots: match[5] * 1,
    maxObsidianRobots: match[7] * 1
  }
});

let executedPaths = 0;
let skippedPaths = 0;

function run(blueprint, minutes, state, shortCircuit) {
  // if (minutes == 0) {
  //   return state.geodes;
  // }
  
  // if (minutes == 1) {
  //   return state.geodes + state.geodeRobots;
  // }
  
  // let bestPossible = state.geodes + (state.geodeRobots * minutes) + (minutes * ((minutes - 1) >> 1));
  // if (bestPossible <= shortCircuit) {
  //   skippedPaths++;
  //   executedPaths--;
  //   console.log("Short circuit")
  //   return shortCircuit;
  // }
  
  let max = 0;
  
  let newOre = state.oreRobots;
  let newClay = state.clayRobots;
  let newObsidian = state.obsidianRobots;
  let newGeodes = state.geodeRobots;
  
  if (minutes == 2) {
    if (blueprint.geodeRobotCost.ore <= state.ore && blueprint.geodeRobotCost.obsidian <= state.obsidian) {
      return state.geodes + (state.geodeRobots * 2) + 1;
    } else {
      return state.geodes + (state.geodeRobots * 2);
    }
  }
    
  for (var i = 0; i < 4; i++) {
    let localState = {
      ore: state.ore,
      clay: state.clay,
      obsidian: state.obsidian,
      geodes: state.geodes,
      oreRobots: state.oreRobots,
      clayRobots: state.clayRobots,
      obsidianRobots: state.obsidianRobots,
      geodeRobots: state.geodeRobots
    };
    

    if (blueprint.geodeRobotCost.ore <= localState.ore && blueprint.geodeRobotCost.obsidian <= localState.obsidian) {
      localState.ore -= blueprint.geodeRobotCost.ore;
      localState.obsidian -= blueprint.geodeRobotCost.obsidian;
      localState.geodeRobots++;
      i = 100; // break out of the loop at the end
    } else if (i == 1) {
      // attempt to buy an ore robot
      if (blueprint.oreRobotCost.ore <= localState.ore && localState.oreRobots < blueprint.maxOreRobots) {
        // check if there is enough time for a purchase to actually matter
        if (blueprint.oreRobotCost.ore + 4 > minutes) {
          skippedPaths++;
          continue;
        }
        
        localState.ore -= blueprint.oreRobotCost.ore;
        localState.oreRobots++;
      } else {
        skippedPaths++;
        continue;
      }
    } else if (i == 2) {
      // attempt to buy a clay robot
      if (blueprint.clayRobotCost.ore <= localState.ore && localState.clayRobots < blueprint.maxClayRobots) {
        if (minutes <= 5) {
          skippedPaths++;
          continue;
        }
        
        localState.ore -= blueprint.clayRobotCost.ore;
        localState.clayRobots++;
      } else {
        skippedPaths++;
        continue;
      }
    } else if (i == 3) {
      if (minutes <= 3) {
        skippedPaths++;
        continue;
      }
      // attempt to buy an obsidian robot
      if (blueprint.obsidianRobotCost.ore <= localState.ore && blueprint.obsidianRobotCost.clay <= localState.clay && localState.obsidianRobots < blueprint.maxObsidianRobots) {
        localState.ore -= blueprint.obsidianRobotCost.ore;
        localState.clay -= blueprint.obsidianRobotCost.clay;
        localState.obsidianRobots++;
      } else {
        skippedPaths++;
        continue;
      }
    } 
    // else if (i == 4) {
    //   // attempt to buy an geode robot
    //   if (blueprint.geodeRobotCost.ore <= localState.ore && blueprint.geodeRobotCost.obsidian <= localState.obsidian) {
    //     localState.ore -= blueprint.geodeRobotCost.ore;
    //     localState.obsidian -= blueprint.geodeRobotCost.obsidian;
    //     localState.geodeRobots++;
    //   } else {
    //     skippedPaths++;
    //     continue;
    //   }
    // }
    
    localState.ore += newOre;
    localState.clay += newClay;
    localState.obsidian += newObsidian;
    localState.geodes += newGeodes;
    
    executedPaths++;
    let thisRun = run(blueprint, minutes - 1, localState, max);
    max = Math.max(thisRun, max);
  }
  
  return max;
}

let initialState = {
  ore: 0,
  clay: 0,
  obsidian: 0,
  geodes: 0,
  oreRobots: 1,
  clayRobots: 0,
  obsidianRobots: 0,
  geodeRobots : 0
}

const DAYS = 20;
const ID = 1;

// console.log(run(blueprints[0], 28, initialState, 0))
blueprints.forEach(blueprint => console.log(run(blueprint, 32, initialState, 0)))

console.log(skippedPaths)
console.log(executedPaths);
