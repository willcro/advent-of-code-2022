const text = await Deno.readTextFile("./input.txt");

const REGEX = /Blueprint (\d+): Each ore robot costs (\d+) ore\. Each clay robot costs (\d+) ore\. Each obsidian robot costs (\d+) ore and (\d+) clay\. Each geode robot costs (\d+) ore and (\d+) obsidian\./

const blueprints = text.split("\n").map(line => REGEX.exec(line)).map(match => {
  return {
    id: match[1] * 1,
    oreRobotCost: {
      ore: match[2] * 1,
      clay: 0,
      obsidian: 0
    },
    clayRobotCost: {
      ore: match[3] * 1,
      clay: 0,
      obsidian: 0
    },
    obsidianRobotCost: {
      ore: match[4] * 1,
      clay: match[5] * 1,
      obsidian: 0
    },
    geodeRobotCost: {
      ore: match[6] * 1,
      clay: 0,
      obsidian: match[7] * 1
    },
  }
});

// function run(blueprint) {



// }

function countGeodes(geodeArrangement) {
  let geodes = 0;
  for (let n = 0; n < 12; n++) {
    let bitter = 1 << n;
    if ((geodeArrangement & bitter) > 0) {
      geodes += (n + 1);
    }
  }

  return geodes;
}

function createGeodeCountToGeodeMap() {
  let out = {};
  for (let i = 0; i < Math.pow(2, 12); i++) {
    let count = countGeodes(i);
    if (out[count]) {
      out[count].push(i);
    } else {
      out[count] = [i];
    }
  }
  return out;
}

function createClayToOreMap(blueprint) {
  var out = {};
  for (var i = 0; i < Math.pow(2, 17); i++) {
    for (var n = 0; n < Math.pow(2, 17); n++) {
      if (oreClayCompatible(i, n, blueprint)) {
        if (out[n]) {
          out[n].push(i);
        } else {
          out[n] = [i];
        }
      }
    }
  }

  return out;
}

function createObsidianToClayMap(blueprint) {
  var clayDelay = blueprint.clayRobotCost.ore;
  var obDelay = clayDelay + triangle(blueprint.obsidianRobotCost.clay);

  var out = {};
  for (var i = 0; i < Math.pow(2, 19 - clayDelay); i++) {
    for (var n = 0; n < Math.pow(2, 21 - obDelay); n++) {
      if (clayObsidianCompatible(i << 2, n, blueprint)) {
        if (out[n]) {
          out[n].push(i);
        } else {
          out[n] = [i];
        }
      }
    }
  }

  return out;
}

function triangle(n) {
  if (n <= 1) {
    return 2;
  }

  if (n <= 3) {
    return 3;
  }

  if (n <= 6) {
    return 4;
  }

  if (n <= 10) {
    return 5;
  }

  if (n <= 15) {
    return 6;
  }

  if (n <= 21) {
    return 7;
  }

  return 8;

}

function createGeodeToObsidianMap(blueprint) {
  var out = {};

  var obDelay = blueprint.clayRobotCost.ore + triangle(blueprint.obsidianRobotCost.clay);
  var geodeDelay = obDelay + triangle(blueprint.geodeRobotCost.obsidian)

  // console.log(obDelay, geodeDelay)

  for (var i = 0; i < Math.pow(2, 21 - obDelay); i++) {
    for (var n = 0; n < Math.pow(2, 23 - geodeDelay); n++) {
      if (obsidianGeodeCompatible(i << 2, n, blueprint)) {
        if (out[n]) {
          out[n].push(i);
        } else {
          out[n] = [i];
        }
      }
    }
  }

  return out;
}

function oreClayCompatible(oreArrangement, clayArrangement, blueprint) {
  let ore = 0;
  let oreBots = 0;
  let lastOreBot = 0;
  let wasted = true;
  for (var i = 18; i >= 0; i--) {
    let oreToAdd = oreBots;
    let bitter = 1 << i;

    if ((oreArrangement & bitter) > 0) {
      oreBots += 1;
      lastOreBot = i;
      wasted = true;
    }

    if ((clayArrangement & bitter) > 0) {
      if (ore < blueprint.clayRobotCost.ore) {
        return 0;
      } else {
        ore -= blueprint.clayRobotCost.ore;
        wasted = clay > (lastOreBot - i - 1);
      }
    }
    ore += oreToAdd;
  }
  return !wasted;
}

function clayObsidianCompatible(clayArrangement, obsidianArrangement, blueprint) {
  let clay = 0;
  let clayBots = 0;
  let lastClayBot = 0;
  let wasted = true;

  if ((obsidianArrangement ^ clayArrangement) != (obsidianArrangement | clayArrangement)) {
    return false;
  }

  for (var i = 18; i >= 0; i--) {
    let clayToAdd = clayBots;
    let bitter = 1 << i;

    if ((clayArrangement & bitter) > 0) {
      clayBots += 1;
      lastClayBot = i;
      wasted = true;
    }

    if ((obsidianArrangement & bitter) > 0) {
      if (clay < blueprint.obsidianRobotCost.clay) {
        return 0;
      } else {
        clay -= blueprint.obsidianRobotCost.clay;
        wasted = clay > (lastClayBot - i - 1)
      }
    }
    clay += clayToAdd;
  }

  return !wasted;
}

function obsidianGeodeCompatible(obsidianArrangement, geodeArrangement, blueprint) {
  let obsidian = 0;
  let obsidianBots = 0;
  let lastObsidianBot = 0;
  let wasted = true;

  if ((obsidianArrangement ^ geodeArrangement) != (obsidianArrangement | geodeArrangement)) {
    return false;
  }

  for (var i = 16; i >= 0; i--) {
    let obsidianToAdd = obsidianBots;
    let bitter = 1 << i;
    // obsidianBots += ((obsidianArrangement & bitter) > 0) * 1;

    if ((obsidianArrangement & bitter) > 0) {
      obsidianBots += 1;
      lastObsidianBot = i;
      wasted = true;
    }

    if ((geodeArrangement & bitter) > 0) {
      if (obsidian < blueprint.geodeRobotCost.obsidian) {
        return 0;
      } else {
        obsidian -= blueprint.geodeRobotCost.obsidian;

        wasted = obsidian > (lastObsidianBot - i - 1)

      }
    }
    obsidian += obsidianToAdd;
  }

  // if (obsidian > (lastObsidianBot - 1)) {
  //   return 0;
  // }

  return !wasted;
}

// console.log(clayObsidianCompatible(0b11111111111111111, 0b00001000000000000, blueprints[0]))

var count = 0;

// console.log(createGeodeToObsidianMap(blueprints[0])[0b10000]);

// console.log(createGeodeCountToGeodeMap())

function countWaysToGetGeodes(blueprint) {
  let geodeToObsidianMap = createGeodeToObsidianMap(blueprint);
  let obsidianToClayMap = createObsidianToClayMap(blueprint);
  let geodeCountToGeodeMap = createGeodeCountToGeodeMap(blueprint);

  // console.log(geodeToObsidianMap)

  // console.log(Object.values(geodeToObsidianMap).length)
  // console.log(Object.values(geodeToObsidianMap).map(it => it.length).reduce((a, b) => a + b))

  // let geodeArrangements = geodeCountToGeodeMap[count];

  for (var i = 1; i < 30; i++) {
    console.log(`Testing with count ${i}`)
    if (!testWithMaps(geodeToObsidianMap, obsidianToClayMap, geodeCountToGeodeMap, blueprint, i)) {
      console.log(`id: ${blueprint.id}, value: ${i - 1}`);
      return i - 1;
    }
  }


  // geodeArrangements.filter(ga => geodeToObsidianMap[ga] != undefined).forEach(geodeArrangement => {
  //   geodeToObsidianMap[geodeArrangement].filter(oa => obsidianToClayMap[oa] != undefined).forEach(obsidianArrangement => {
  //     obsidianToClayMap[obsidianArrangement].forEach(clayArrangement => {
  //       if (c % 1000 == 0) {
  //         console.log(c);
  //       }
  //       testArrangements(geodeArrangement, obsidianArrangement, clayArrangement, blueprint);
  //       c++;
  //     })
  //   })
  // });

  // return geodeArrangements.flatMap(geodeArrangement => geodeToObsidianMap[geodeArrangement])
  //   .map(obs => obsidianToClayMap[obs] ? obsidianToClayMap[obs].length : 0).reduce((a, b) => a + b);

}

function testWithMaps(geodeToObsidianMap, obsidianToClayMap, geodeCountToGeodeMap, blueprint, count) {
  let geodeArrangements = geodeCountToGeodeMap[count];
  let c = 0;

  for (let ga = geodeArrangements.length - 1; ga >= 0; ga--) {
    let geodeArrangement = geodeArrangements[ga];
    let obsidianArrangements = geodeToObsidianMap[geodeArrangement];
    if (obsidianArrangements == undefined) {
      continue;
    }

    for (let oa = obsidianArrangements.length - 1; oa >= 0; oa--) {
      let obsidianArrangement = obsidianArrangements[oa];
      let clayArrangements = obsidianToClayMap[obsidianArrangement];
      if (clayArrangements == undefined) {
        continue;
      }

      for (let ca = clayArrangements.length - 1; ca >= 0; ca--) {
        // if (c % 1000 == 0) {
        //   console.log(c);
        // }
        let clayArrangement = clayArrangements[ca];
        if (testArrangements(geodeArrangement, obsidianArrangement, clayArrangement, blueprint)) {
          return 1;
        }
        c++;
      }
    }
  }
  return 0;
}

function testArrangements(geodeArrangement, obsidianArrangement, clayArrangement, blueprint) {
  // console.log(clayArrangement.toString(2), obsidianArrangement.toString(2), geodeArrangement.toString(2));

  let oreDelay = blueprint.oreRobotCost.ore - 2;

  for (var i = 0; i < Math.pow(2, 17 - (2 * oreDelay)); i++) {
    if (testArrangement(geodeArrangement, obsidianArrangement << 2, clayArrangement << 4, i << 4, blueprint)) {

      // console.log(geodeArrangement.toString(2), (obsidianArrangement << 2).toString(2), (clayArrangement << 4).toString(2), (i << (4 + oreDelay)).toString(2))
      // console.log(i.toString(2), clayArrangement.toString(2), obsidianArrangement.toString(2), geodeArrangement.toString(2));

      return true;
    }
  }

  return false;
}

function testArrangement(geodeArrangement, obsidianArrangement, clayArrangement, oreArrangement, blueprint) {
  let ore = 2;
  let oreBots = 1;

  let condition = (geodeArrangement ^ obsidianArrangement) == (geodeArrangement | obsidianArrangement)
    && (geodeArrangement ^ clayArrangement) == (geodeArrangement | clayArrangement)
    && (geodeArrangement ^ oreArrangement) == (geodeArrangement | oreArrangement)
    && (obsidianArrangement ^ clayArrangement) == (obsidianArrangement | clayArrangement)
    && (obsidianArrangement ^ oreArrangement) == (obsidianArrangement | oreArrangement)
    && (clayArrangement ^ oreArrangement) == (clayArrangement | oreArrangement);

  if (!condition) {
    return false;
  }

  for (var i = 20; i >= 0; i--) {
    let oreToAdd = oreBots;
    let bitter = 1 << i;
    oreBots += ((oreArrangement & bitter) > 0) * 1;

    ore -= ((oreArrangement & bitter) > 0) * blueprint.oreRobotCost.ore;
    ore -= ((clayArrangement & bitter) > 0) * blueprint.clayRobotCost.ore;
    ore -= ((obsidianArrangement & bitter) > 0) * blueprint.obsidianRobotCost.ore;
    ore -= ((geodeArrangement & bitter) > 0) * blueprint.geodeRobotCost.ore;

    if (ore < 0) {
      return 0;
    }

    ore += oreToAdd;
  }
  return 1;

}

function part1() {
  return blueprints.map((b,i) => countWaysToGetGeodes(b) * (i + 1)).reduce((a,b) => a + b);
}

// console.log(testArrangement(0b100100, 0b1000100000000, 0b101010000100000000000, 0b0, blueprints[0]))

// console.log(countWaysToGetGeodes(blueprints[1]))

console.log(part1())

// console.log(createClayToOreMap(blueprints[0]));