// I am leaving this here just in case I need something in it
// There was good thoughts in here, but ultimately, it was not a good
// way to solve the problem

const text = await Deno.readTextFile("./input.txt");

const sensors = text.split("\n")
  .map(line => /Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)/.exec(line))
  .map(match => {
    return {
      sensorX: match[1] * 1,
      sensorY: match[2] * 1,
      beaconX: match[3] * 1,
      beaconY: match[4] * 1
    }
  });

function part1(sensors) {
  let beaconSet = new Set();
  sensors.map(s => `${s.beaconX},${s.beaconY}`).forEach(it => beaconSet.add(it));
  let sensorPolys = sensors.map(sensorToPolygon);
  let y = 2000000;
  console.log(sensorPolys)
  let min = sensorPolys.map(it => it.left.x).reduce((a, b) => Math.min(a, b));
  let max = sensorPolys.map(it => it.right.x).reduce((a, b) => Math.max(a, b));
  let out = 0;

  for (let x = min; x <= max; x++) {
    if (sensorPolys.some(poly => pointInPolygon(poly, { x: x, y: y })) && !beaconSet.has(`${x},${y}`)) {
      out++;
    }
  }
  return out;
}

function search(uberPoly, sensors) {
  
  uberPoly.area = area(uberPoly);

  let sensorPolys = sensors.map(sensorToPolygon);
  let all = [];
  sensorPolys.forEach(p => all.push(p));
  all.push(uberPoly);

  for (let p1 = 0; p1 < sensorPolys.length; p1++) {
    let poly1 = sensorPolys[p1];
    let newPolys = [];
    // console.log(all)
    for (let p2 = p1 + 1; p2 < all.length; p2++) {
      let poly2 = all[p2];
      if (poly2.ids.includes(poly1.ids[0])) {
        continue;
      }

      let newPoly = overlap(poly1, poly2);
      if (newPoly != null) {
        newPolys.push(newPoly);
      }
    }

    newPolys.forEach(it => all.push(it));
  }

  return all.filter(it => it.ids.includes(-1))
    // .filter(it => it.ids.length > 1)
    .map(it => it.ids.length % 2 == 0 ? it.area : -it.area)
    .reduce((a, b) => a + b);

  // return all;
}

function manhattan(x1, y1, x2, y2) {
  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

function sensorToPolygon(sensor, index) {
  const dist = manhattan(sensor.sensorX, sensor.sensorY, sensor.beaconX, sensor.beaconY);
  return {
    top: {
      x: sensor.sensorX,
      y: sensor.sensorY - dist
    },
    bottom: {
      x: sensor.sensorX,
      y: sensor.sensorY + dist
    },
    left: {
      x: sensor.sensorX - dist,
      y: sensor.sensorY
    },
    right: {
      x: sensor.sensorX + dist,
      y: sensor.sensorY
    },
    ids: [index]
  }
}

// point1 and point2 should form a diagonal line
// -1 if above, 1 if below, 0 if on line
function aboveOrBelow(point1, point2, testPoint) {
  let slope = (point2.y - point1.y) / (point2.x - point1.x);
  let intercept = point1.y - (slope * point1.x);
  return ((slope * testPoint.x) + intercept) - testPoint.y;
}

function pointInPolygon(polygon, point) {

  return aboveOrBelow(polygon.top, polygon.left, point) <= 0
    && aboveOrBelow(polygon.top, polygon.right, point) <= 0
    && aboveOrBelow(polygon.bottom, polygon.left, point) >= 0
    && aboveOrBelow(polygon.bottom, polygon.right, point) >= 0;
}

function lineOverlap(line1Point1, line1Point2, line2Point1, line2Point2) {
  let slope1 = (line1Point2.y - line1Point1.y) / (line1Point2.x - line1Point1.x);
  let intercept1 = line1Point1.y - (slope1 * line1Point1.x);

  let slope2 = (line2Point2.y - line2Point1.y) / (line2Point2.x - line2Point1.x);
  let intercept2 = line2Point1.y - (slope2 * line2Point1.x);

  if (slope1 == slope2) {
    return null;
  }

  let interceptX = (intercept2 - intercept1) / (slope1 - slope2);

  // verify that point is between both lines
  let inLine1 = interceptX <= Math.max(line1Point1.x, line1Point2.x) && interceptX >= Math.min(line1Point1.x, line1Point2.x);
  let inLine2 = interceptX <= Math.max(line2Point1.x, line2Point2.x) && interceptX >= Math.min(line2Point1.x, line2Point2.x);

  if (inLine1 && inLine2) {
    return {
      x: interceptX,
      y: (slope1 * interceptX) + intercept1
    }
  } else {
    return null;
  }

}

function overlap(poly1, poly2) {
  let points1 = [poly1.top, poly1.bottom, poly1.left, poly1.right];
  let points2 = [poly2.top, poly2.bottom, poly2.left, poly2.right];

  let lines1 = [
    [poly1.top, poly1.left],
    [poly1.top, poly1.right],
    [poly1.bottom, poly1.left],
    [poly1.bottom, poly1.right]
  ];

  let lines2 = [
    [poly2.top, poly2.left],
    [poly2.top, poly2.right],
    [poly2.bottom, poly2.left],
    [poly2.bottom, poly2.right]
  ];

  let oneInTwo = points1.filter(point => pointInPolygon(poly2, point));
  let twoInOne = points2.filter(point => pointInPolygon(poly1, point));

  let lineOverlaps = lines1.flatMap(line1 => lines2.map(line2 => lineOverlap(line1[0], line1[1], line2[0], line2[1])))
    .filter(it => it != null);

  let pointSet = new Set();

  oneInTwo.map(p => JSON.stringify(p)).forEach(p => pointSet.add(p));
  twoInOne.map(p => JSON.stringify(p)).forEach(p => pointSet.add(p));
  lineOverlaps.map(p => JSON.stringify(p)).forEach(p => pointSet.add(p));

  if (pointSet.size == 0) {
    return null;
  }

  // console.log(pointSet)
  return pointsToPolygon(Array.from(pointSet).map(it => JSON.parse(it)), poly1.ids.concat(poly2.ids));
}

function pointsToPolygon(points, ids) {
  let poly = {
    top: points.reduce((a, b) => a.y < b.y ? a : b),
    bottom: points.reduce((a, b) => a.y > b.y ? a : b),
    left: points.reduce((a, b) => a.x < b.x ? a : b),
    right: points.reduce((a, b) => a.x > b.x ? a : b),
    ids: ids
  };
  poly.area = area(poly);
  return poly;
}

function area(polygon) {
  let height = 2 * (Math.min(polygon.right.y, polygon.left.y) - polygon.top.y) + 1;
  let width = Math.abs(polygon.top.x - polygon.bottom.x) + 1;
  let something = Math.floor(height / 2)
  let triangle = height % 2 == 0 ? something * (something - 1) : something * something;
  return (height * width) + (triangle * 2);
}

// console.log(part1(sensors))

// let uberPoly = {
//   top: {
//     x: 2000000,
//     y: 0
//   },
//   bottom: {
//     x: 2000000,
//     y: 4000000
//   },
//   left: {
//     x: 0,
//     y: 2000000
//   },
//   right: {
//     x: 4000000,
//     y: 2000000
//   },
//   ids: [-1]
// };
// uberPoly.area = area(uberPoly);

function part2(sensors, max) {
  let sensorPolys = sensors.map(sensorToPolygon);

  
  for (let i = 0; i < sensorPolys.length; i++) {
    let sensor = sensorPolys[i];
    let x = sensor.top.x;
    let y = sensor.top.y - 1;
    
    // walk clockwise
    
    // walk down top-right
    while (y < sensor.right.y) {
      if (x <= max && y <= max && y>=0 && x>=0 && sensorPolys.every(poly => !pointInPolygon(poly, { x: x, y: y }))) {
        return x * 4000000 + y;
      }
      x++;
      y++;
    }
    
    // walk down bottom-right
    while (x > sensor.bottom.x) {
      if (x <= max && y <= max && y >= 0 && x >= 0 && sensorPolys.every(poly => !pointInPolygon(poly, { x: x, y: y }))) {
        return x * 4000000 + y;
      }
      x--;
      y++;
    }
    
    // walk up bottom-left
    while (x > sensor.left.x) {
      if (x <= max && y <= max && y >= 0 && x >= 0 && sensorPolys.every(poly => !pointInPolygon(poly, { x: x, y: y }))) {
        return x * 4000000 + y;
      }
      x--;
      y--;
    }
    
    // walk up top-left
    while (x > sensor.top.x) {
      if (x <= max && y <= max && y >= 0 && x >= 0 && sensorPolys.every(poly => !pointInPolygon(poly, { x: x, y: y }))) {
        return x * 4000000 + y;
      }
      x++;
      y--;
    }
    
  }
  
}


// console.log(sensorPolys.every(poly => !pointInPolygon(poly, { x: 14, y: 11 })))
console.log(part2(sensors, 4000000))

