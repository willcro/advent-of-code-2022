const text = await Deno.readTextFile("./input.txt");

const sensors = text.split("\n")
  .map(line => /Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)/.exec(line))
  .map(match => {
    return {
      sensorX: match[1] * 1,
      sensorY: match[2] * 1,
      beaconX: match[3] * 1,
      beaconY: match[4] * 1,
      radius: Math.abs(match[1] - match[3]) + Math.abs(match[2] - match[4])
    }
  });

function part1(sensors, row) {
  let beaconSet = new Set();
  sensors.map(s => `${s.beaconX},${s.beaconY}`).forEach(it => beaconSet.add(it));
  let y = row;
  let min = sensors.map(it => it.sensorX - it.radius).reduce((a, b) => Math.min(a, b));
  let max = sensors.map(it => it.sensorX + it.radius).reduce((a, b) => Math.max(a, b));
  let out = 0;

  for (let x = min; x <= max; x++) {
    if (sensors.some(sensor => pointInSensorRadius(sensor, x, y)) && !beaconSet.has(`${x},${y}`)) {
      out++;
    }
  }
  return out;
}

function manhattan(x1, y1, x2, y2) {
  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

function pointInSensorRadius(sensor, x, y) {
  return manhattan(x, y, sensor.sensorX, sensor.sensorY) <= sensor.radius;
}

function part2(sensors, max) {

  for (let i = 0; i < sensors.length; i++) {
    let sensor = sensors[i];
    let x = sensor.sensorX;
    let y = sensor.sensorY - sensor.radius - 1;
    const test = () => x <= max && y <= max
      && y >= 0 && x >= 0 
      && sensors.every(s => !pointInSensorRadius(s, x, y));

    // walk clockwise
    // walk down top-right
    while (y < sensor.sensorY) {
      if (test()) {
        return x * 4000000 + y;
      }
      x++;
      y++;
    }

    // walk down bottom-right
    while (y < sensor.sensorY + sensor.radius) {
      if (test()) {
        return x * 4000000 + y;
      }
      x--;
      y++;
    }

    // walk up bottom-left
    while (y < sensor.sensorY) {
      if (test()) {
        return x * 4000000 + y;
      }
      x--;
      y--;
    }

    // walk up top-left
    while (y > sensor.sensorY - sensor.radius) {
      if (test()) {
        return x * 4000000 + y;
      }
      x++;
      y--;
    }

  }

}
console.log(part1(sensors, 2000000))
console.log(part2(sensors, 4000000))

