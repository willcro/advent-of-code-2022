const text = await Deno.readTextFile("./input.txt");

text.split("\n").map(it => it.split(": ")[0] + " = () => " + it.split(": ")[1].replaceAll(/(\w{4})/g, "$1()")).forEach(eval)

// part1
console.log(root())

text.split("\n").map(it => {
  let variable = it.split(": ")[0];
  let value = it.split(": ")[1].replaceAll(/(\w{4})/g, "${$1()}")

  return variable + " = () => `(" + value + ")`"
}).forEach(eval)

humn = () => "'x'"

root = () => `${pgnv()} == ${wcnp()}`

let equation = root();

equation = equation.replaceAll(/\((\d+)\)/g, "$1");

while (/\(\d+ . \d+\)/.exec(equation)) {
  equation = equation.replaceAll(/\(\d+ . \d+\)/g, (it) => eval(it));
}

equation = equation.replaceAll(" ", "")
equation = equation.replaceAll("'x'", "x")
console.log(equation)

// I kinda cheated at this point by just putting it into a graphing calculator
