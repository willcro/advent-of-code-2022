// part 1

const text = await Deno.readTextFile("./input.txt");
var elves = text.split("\n\n").map(it => it.split("\n").map(it => it * 1));

var cals = elves.map(elf => elf.reduce((a, b) => a + b, 0))

// answer to part 1
var topCals = cals.sort((a, b) => b - a);
console.log(topCals[0])

// answer to part 2
console.log(topCals[0] + topCals[1] + topCals[2])

