const text = await Deno.readTextFile("./input.txt");

const instructions = text.split("").map(it => it == "<" ? -1 : 1);

let shapes = [
  {
    id: "-",
    pixels: [
      { x: 0, y: 0 },
      { x: 1, y: 0 },
      { x: 2, y: 0 },
      { x: 3, y: 0 }
    ]
  },
  {
    id: "+",
    pixels: [
      { x: 1, y: 0 },
      { x: 0, y: 1 },
      { x: 1, y: 1 },
      { x: 2, y: 1 },
      { x: 1, y: 2 }
    ]
  },
  {
    id: "J",
    pixels: [
      { x: 0, y: 0 },
      { x: 1, y: 0 },
      { x: 2, y: 0 },
      { x: 2, y: 1 },
      { x: 2, y: 2 }
    ]
  },
  {
    id: "J",
    pixels: [
      { x: 0, y: 0 },
      { x: 0, y: 1 },
      { x: 0, y: 2 },
      { x: 0, y: 3 }
    ]
  },
  {
    id: "[]",
    pixels: [
      { x: 0, y: 0 },
      { x: 1, y: 0 },
      { x: 0, y: 1 },
      { x: 1, y: 1 }
    ]
  }
]

function tetris(instructions, maxShapes) {
  let maxY = -1;
  let rocks = new Set();
  let shapeIndex = 0;

  function getNext() {
    let shape = shapes[shapeIndex % shapes.length].pixels.map(pixel => {
      return {
        x: pixel.x + 2,
        y: pixel.y + maxY + 4
      }
    });
    shapeIndex++;
    return shape;
  }

  function coordToString(coord) {
    return `${coord.x},${coord.y}`;
  } 
  
  function outOfBounds(pixel) {
    return pixel.x < 0 || pixel.x > 6 || pixel.y < 0 || rocks.has(coordToString(pixel));
  }

  let current = getNext();
  let currentOffSet = {x:0, y:0};
  let offsetHistory = {};
  let offsetRollingHistory = [];

  for (let i = 0; i < Infinity; i++) {

    // move left/right
    let direction = instructions[i % instructions.length];
    let nextPos = current.map(pixel => {
      return {
        x: pixel.x + direction,
        y: pixel.y
      }
    });

    if (!nextPos.some(outOfBounds)) {
      current = nextPos;
      currentOffSet = {
        x: currentOffSet.x + direction,
        y: currentOffSet.y
      };
    }

    // move down
    nextPos = current.map(pixel => {
      return {
        x: pixel.x,
        y: pixel.y - 1
      }
    });
    if (!nextPos.some(outOfBounds)) {
      current = nextPos;
      currentOffSet = {
        x: currentOffSet.x,
        y: currentOffSet.y - 1
      };
    } else {
      maxY = current.map(p => p.y).reduce((a, b) => Math.max(a, b, maxY));
      current.map(pixel => `${pixel.x},${pixel.y}`).forEach(p => rocks.add(p));
      if (shapeIndex == maxShapes) {
        return maxY + 1;
      }
      
      let history = offsetHistory[offsetRollingHistory.map(coordToString).join("|")];
      if (history) {        
        let repeatHeight = maxY - history.height;
        let repeatIndex = shapeIndex - history.shapeIndex;
        
        if ((shapeIndex % repeatIndex) == maxShapes % repeatIndex) {
          return ((Math.floor(maxShapes / repeatIndex) - Math.floor(shapeIndex / repeatIndex)) * repeatHeight) + maxY + 1;
        }
        
      }
      
      offsetHistory[offsetRollingHistory.map(coordToString).join("|")] = {
        shapeIndex: shapeIndex,
        height: maxY
      };
      
      offsetRollingHistory = offsetRollingHistory.slice(Math.max(offsetRollingHistory.length - 10, 0));
      offsetRollingHistory.push(currentOffSet);
            
      currentOffSet = { x: 0, y: 0 };
      
      current = getNext();
    }
  }

}

console.log(tetris(instructions, 2022))

console.log(tetris(instructions, 1000000000000))
