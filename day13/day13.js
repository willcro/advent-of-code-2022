const text = await Deno.readTextFile("./input.txt");

const pairs = text.split("\n\n")
  .map(pair => pair.split("\n")
    .map(line => JSON.parse(line)));


// return negative number if a is smaller
// return positive number if b is smaller
function compare(a, b) {
  if (a == undefined) {
    return -1;
  }

  if (b == undefined) {
    return 1;
  }

  if (typeof (a) == 'number' && typeof (b) == 'number') {
    return a - b;
  }

  if (typeof (a) == 'number' && typeof (b) == 'object') {
    a = [a];
  }

  if (typeof (a) == 'object' && typeof (b) == 'number') {
    b = [b];
  }

  for (let i = 0; i < Math.max(a.length, b.length); i++) {
    let c = compare(a[i], b[i]);
    if (c != 0) {
      return c;
    }
  }

  return 0;
}

//part 1
const part1 = pairs.map(pair => compare(pair[0], pair[1]))
  .map((c, i) => c < 0 ? i + 1 : null)
  .filter(it => it != null)
  .reduce((a, b) => a + b);

console.log(part1);

pairs.push([[[2]], [[6]]]);
const sorted = pairs.flat().sort(compare).map(it => JSON.stringify(it));

//part 2
console.log((sorted.indexOf("[[2]]") + 1) * (sorted.indexOf("[[6]]") + 1))
