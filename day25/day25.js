const text = await Deno.readTextFile("./input.txt");

const map = {
  "2": 2,
  "1": 1,
  "0": 0,
  "-": -1,
  "=": -2
};

const map2 = {
  2: "2",
  1: "1",
  0: "0",
  '-1': "-",
  '-2': "="
};

function decimalToSnafu(decimal) {
  let out = "";
  let pow = 1;
  
  while (decimal) {
    let digit = decimal % 5;
    
    if (digit >= 3) {
      digit -= 5;
    }
        
    out = map2[digit] + out;
    decimal = (decimal - digit) / 5;
  }
  return out;

}

function snafuToDecimal(snafu) {
  return snafu.split("").reverse().map((c, i) => map[c] * Math.pow(5, i))
  .reduce((a, b) => a + b);
  
}

function part1() {
  return decimalToSnafu(text.split("\n").map(snafuToDecimal).reduce((a,b) => a + b));
}

console.log(part1())

