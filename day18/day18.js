const text = await Deno.readTextFile("./input.txt");

let voxels = text.split("\n")
  .map(line => line.split(","))
  .map(line => {
    return {
      x: line[0] * 1,
      y: line[1] * 1,
      z: line[2] * 1
    }
  })
  .sort((a, b) => (a.x + a.y + a.z) - (b.x + b.y + b.z));

let max = text.split("\n")
  .flatMap(line => line.split(",").map(it => it * 1)).reduce((a,b) => Math.max(a, b)) + 1;
  
function part1(voxels) {
  let faces = new Set();

  for (let i = 0; i < voxels.length; i++) {
    let voxel = voxels[i];

    let thisFaces = [
      { x: voxel.x, y: voxel.y, z: voxel.z, side: "top" },
      { x: voxel.x, y: voxel.y, z: voxel.z, side: "right" },
      { x: voxel.x, y: voxel.y, z: voxel.z, side: "front" },
      { x: voxel.x, y: voxel.y, z: voxel.z - 1, side: "top" },
      { x: voxel.x - 1, y: voxel.y, z: voxel.z, side: "right" },
      { x: voxel.x, y: voxel.y - 1, z: voxel.z, side: "front" }
    ];

    thisFaces.map(it => JSON.stringify(it)).forEach(face => {
      if (faces.has(face)) {
        faces.delete(face);
      } else {
        faces.add(face);
      }
    })
  }

  return faces.size;
}

console.log(part1(voxels));

function findBubbles(voxels) {
  let grid = new Set([...Array(max).keys()].flatMap(x => [...Array(max).keys()].flatMap(y => [...Array(max).keys()].map(z => `${x},${y},${z}`))));
  voxels.map(v => `${v.x},${v.y},${v.z}`).forEach(v => grid.delete(v));
  let toSearch = [{ x: 0, y: 0, z: 0 }];

  while (toSearch.length > 0) {
    let voxel = toSearch.pop();
    let voxelStr = voxelToString(voxel);

    grid.delete(voxelStr);

    [
      [0, 0, 1],
      [0, 0, -1],
      [0, 1, 0],
      [0, -1, 0],
      [1, 0, 0],
      [-1, 0, 0],
    ].forEach(transform => {
      let newVoxel = {
        x: voxel.x + transform[0],
        y: voxel.y + transform[1],
        z: voxel.z + transform[2],
      }
      if (grid.has(voxelToString(newVoxel))) {
        toSearch.push(newVoxel);
      }
    })

  }

  return Array.from(grid).map(v => v.split(",")).map(line => {
    return {
      x: line[0] * 1,
      y: line[1] * 1,
      z: line[2] * 1
    }
  }).sort((a, b) => (a.x + a.y + a.z) - (b.x + b.y + b.z));

}

function voxelToString(voxel) {
  return `${voxel.x},${voxel.y},${voxel.z}`
}

function part2() {
  return part1(voxels) - part1(findBubbles(voxels))
}

console.log(part2())