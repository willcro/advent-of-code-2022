const text = await Deno.readTextFile("./input.txt");

function findStart(text, size) {
  for (var i=0; i<text.length-size; i++) {
    const slice = text.slice(i, i+size);
    if (new Set(slice.split("")).size == size) {
      return i + size;
    }
  }
}

// part 1
console.log(findStart(text, 4))

//part 2
console.log(findStart(text, 14))

