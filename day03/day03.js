// part 1

const text = await Deno.readTextFile("./input.txt");

const packs = text.split("\n");

const out = packs.map(pack => [
  pack.substring(0, pack.length / 2).split(""),
  pack.substring(pack.length / 2).split("")
]).map(sections => sections[0].filter(it => sections[1].includes(it))[0])
.map(letter => letter.charCodeAt(0))
.map(value => value > 96 ? value - 96 : value - 38)
.reduce((a,b) => a+b, 0);


console.log(out);

const lines = text.split("\n")

let groups = []

let group = []

for (let i = 0; i < lines.length; i++) {
  group.push(lines[i].split(""));
  if (i % 3 == 2) {
    groups.push(group);
    group = [];
  }
}

const out2 = groups.map(g => g[0].filter(i => g[1].includes(i)).filter(i => g[2].includes(i))[0])
  .map(letter => letter.charCodeAt(0))
  .map(value => value > 96 ? value - 96 : value - 38)
  .reduce((a, b) => a + b, 0);

console.log(out2)