const text = await Deno.readTextFile("./input.txt");

const stacksText = text.split("\n\n")[0];
const instructionsText = text.split("\n\n")[1]

const stacksInverse = stacksText.split("\n")
  .slice(0, -1)
    // regex is different for practice
  .map(line => /.(.). .(.). .(.). .(.). .(.). .(.). .(.). .(.). .(.)./.exec(line).slice(1));

// abc    khea
// efg => lifb
// hij    mjgc
// klm
function rotate(matrix) {
  let out = [];
  for (let x=0; x<matrix[0].length; x++) {
    let row = []
    for (let y = matrix.length - 1; y >= 0; y--) {
      row.push(matrix[y][x])
    }
    out.push(row);
  }
  return out;
}

let stacks = rotate(stacksInverse).map(stack => stack.filter(it => it != " "))

const instructions = instructionsText
  .split("\n")
  .map(it => /move (\d+) from (\d+) to (\d+)/.exec(it).slice(1).map(it => it * 1))
  .map(instruction => {
    return {
      count: instruction[0],
      from: instruction[1] - 1, //subract one because instructions are 1-indexed
      to: instruction[2] - 1
    }
  });
    
    
function execute(stacks, instructions) {
  for (let i=0; i<instructions.length; i++) {
    let instruction = instructions[i];
    let take = stacks[instruction.from].slice(-instruction.count);
    stacks[instruction.from] = stacks[instruction.from].slice(0, -instruction.count);
    stacks[instruction.to] = stacks[instruction.to].concat(take); // change take to take.reverse() for part one
  }
  return stacks;
}

let out = execute(stacks, instructions);

console.log(out.map(stack => stack[stack.length-1]));