const text = await Deno.readTextFile("./input.txt");
// remove the first line to avoid having to parse that
const lines = text.split("\n").slice(1);

const CD_REGEX = /\$ cd ([a-z]+.*)/;
const CD_UP_REGEX = /\$ cd \.\./;
const LS_REGEX = /\$ ls/;
const FILE_REGEX = /(\d+) (.*)/;
const DIR_REGEX = /dir (.*)/;

function createTree(lines) {
  let out = createDirectory();
  for (let i=0; i<lines.length; i++) {
    const line = lines[i];
    if (CD_REGEX.test(line)) {
      const match = CD_REGEX.exec(line);
      if (out.directories[match[1]]) {
        throw "directory already existed";
      }
      out.directories[match[1]] = createTree(lines.slice(i+1));
      i += out.directories[match[1]].line + 1
    } else if (CD_UP_REGEX.test(line)) {
      out.line = i;
      return out;
    } else if (LS_REGEX.test(line)) {
      
    } else if (FILE_REGEX.test(line)) {
      const match = FILE_REGEX.exec(line);
      out.files[match[2]] = {
        name: match[2],
        size: match[1] * 1
      };
    } else if (DIR_REGEX.test(line)) {
      
    } else {
      throw `line "${line}" didn't match regex`
    }
  }
  return out;
}

function createDirectory() {
  return {
    directories: {},
    files: {}
  };
}


function populateSizes(tree) {
  var size = 0;
  for (const dir in tree.directories) {
    populateSizes(tree.directories[dir]);
    size += tree.directories[dir].size;
  }
  
  for (const file in tree.files) {
    size += tree.files[file].size;
  }
  
  tree.size = size;
  return tree;
}

function sumOfDirSizes(tree, max) {
  let sum = 0;
  for (const dir in tree.directories) {
    sum += sumOfDirSizes(tree.directories[dir], max);
    if (tree.directories[dir].size <= max) {
      sum += tree.directories[dir].size;
    }
  }
  return sum;
}

console.log(sumOfDirSizes(populateSizes(createTree(lines)), 100000));

function listSizes(tree) {
  let out = [tree.size];
  for (const dir in tree.directories) {
    out = out.concat(listSizes(tree.directories[dir]));
  }
  
  return out.sort((a,b) => a-b);
}

let tree = populateSizes(createTree(lines));

let amountToFree = 30000000 - (70000000 - tree.size)

// part 2
console.log(listSizes(tree).filter(it => it >= amountToFree)[0]);
