const text = await Deno.readTextFile("./input.txt");

// let original = text.split("\n").map(it => it * 1);

let nums = text.split("\n").map((it, i) => {
  return {
    value: it * 811589153,
    position: i
  }
});


for (let i = 0; i<10; i++) {
  console.log(i);
  nums.forEach(num => {
    let oldPos = num.position;
      
    let newPos = getNewPos2(oldPos, num.value);
    
    if (oldPos < newPos) {
      nums.filter(it => it.position > oldPos && it.position <= newPos).forEach(it => it.position--);
    }
    
    if (oldPos > newPos) {
      nums.filter(it => it.position < oldPos && it.position >= newPos).forEach(it => it.position++);
    }
    
    num.position = newPos;
  
  })
}

function getNewPos2(oldPos, add) {
  let newPos = oldPos + add;
  
  if (newPos >= nums.length) {
    let times = Math.floor(newPos / nums.length);
    
    newPos = newPos - (times * nums.length) + times;
    return getNewPos2(newPos, 0);
  }
  
  if (newPos < 0) {
    let times = Math.ceil(Math.abs(newPos) / nums.length);

    newPos = newPos + (times * nums.length) - times;
    return getNewPos2(newPos, 0);
  }
  
  return newPos;
  
}

function getNewPos(oldPos, add) {
  let newPos = oldPos + add;
  
  while (newPos >= nums.length || newPos < 0) {
    if (newPos >= nums.length) {
      newPos = newPos - nums.length + 1;
    }
    
    if (newPos < 0) {
      newPos = newPos + nums.length - 1;
    }
  }
  
  return newPos;
}

console.log(nums.sort((a,b) => a.position - b.position).map(it => it.value));


let zeroIndex = nums.find(it => it.value == 0).position;

let oneThousand = nums.find(it => it.position == (zeroIndex + 1000) % nums.length).value;

let twoThousand = nums.find(it => it.position == (zeroIndex + 2000) % nums.length).value;

let threeThousand = nums.find(it => it.position == (zeroIndex + 3000) % nums.length).value;

console.log(oneThousand + twoThousand + threeThousand)


