// lol this code is very slow

const text = await Deno.readTextFile("./input.txt");

function letterToHeight(letter) {
  if (letter == "S") {
    return 0;
  } else if (letter == "E") {
    return 25;
  } else {
    return letter.charCodeAt(0) - 97;
  }
}

function findLetter(letters, letter) {
  for (let y = 0; y < letters.length; y++) {
    for (let x = 0; x < letters[y].length; x++) {
      if (letters[y][x] == letter) {
        return `${x},${y}`;
      }
    }
  }
  throw "Not found";
}

let letters = text.split("\n").map(line => line.split(""));
let start = findLetter(letters, "S");
let end = findLetter(letters, "E");

let heights = letters.map(row => row.map(letterToHeight));

function findLowest(vertices) {
  return Object.entries(vertices).reduce((a, b) => a[1].dist < b[1].dist ? a : b)[0];
}

function dijkstra(heights) {
  let vertices = {};
  heights.forEach((row, y) => row.forEach((height, x) => {
    vertices[`${x},${y}`] = {
      dist: x == start.x && y == start.y ? 0 : Infinity,
      prev: null,
      height: height,
      x: x,
      y: y
    }
  }))

  vertices[start].dist = 0;

  while (Object.entries(vertices).length > 0) {
    let vertCoord = findLowest(vertices);
    let vert = vertices[vertCoord];
    delete vertices[vertCoord];

    if (vertCoord == end) {
      return vert.dist;
    }

    [
      [0, -1], // up
      [0, 1],  // down
      [-1, 0], // left
      [1, 0]   // right
    ].forEach(transform => {
      var next = vertices[`${vert.x + transform[0]},${vert.y + transform[1]}`];
      if (next && next.height - vert.height <= 1 && vert.dist + 1 < next.dist) {
        next.dist = vert.dist + 1;
        next.prev = vertCoord;
      }
    });
  }

  throw "We didn't reach the end";

}

console.log(dijkstra(heights))

function dijkstraPart2(heights) {
  let vertices = {};
  heights.forEach((row, y) => row.forEach((height, x) => {
    vertices[`${x},${y}`] = {
      dist: x == start.x && y == start.y ? 0 : Infinity,
      prev: null,
      height: height,
      x: x,
      y: y
    }
  }))

  // we will be working backwards, so we start at the end
  vertices[end].dist = 0;

  while (Object.entries(vertices).length > 0) {

    let vertCoord = findLowest(vertices);
    let vert = vertices[vertCoord];
    delete vertices[vertCoord];

    if (vert.height == 0) {
      return vert.dist;
    }

    [
      [0, -1], // up
      [0, 1],  // down
      [-1, 0], // left
      [1, 0]   // right
    ].forEach(transform => {
      var next = vertices[`${vert.x + transform[0]},${vert.y + transform[1]}`];
      if (next && next.height - vert.height >= -1 && vert.dist + 1 < next.dist) {
        next.dist = vert.dist + 1;
        next.prev = vertCoord;
      }
    });
  }

  throw "We didn't reach the end";
}

console.log(dijkstraPart2(heights))
