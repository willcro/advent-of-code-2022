const text = await Deno.readTextFile("./input.txt");

const ADDX_REGEX = /addx (.+)/
const NOOP_REGEX = /noop/

const instructions = text.split("\n");

function run(instructions) {
  let x = 1;
  let cycle = 0;
  let signal = 0;
  let screen = "";

  let runCycle = () => {
    cycle++;
    if ([20, 60, 100, 140, 180, 220].includes(cycle)) {
      signal += cycle * x;
    }
    if ((cycle - 1) % 40 == 0) {
      screen += "\n";
    }
    if (Math.abs(((cycle - 1) % 40) - x) <= 1) {
      screen += "#";
    } else {
      screen += " "; // I used spaces instead of periods
    }
  }

  for (let i = 0; i < instructions.length; i++) {
    let instruction = instructions[i];
    if (ADDX_REGEX.test(instruction)) {
      let match = ADDX_REGEX.exec(instruction);
      runCycle();
      runCycle();
      let adder = match[1] * 1;
      x += adder;
    } else if (NOOP_REGEX.test(instruction)) {
      runCycle();
    } else {
      throw "Unknown command " + instruction;
    }
  }

  console.log(screen);
  return signal;
}

console.log(run(instructions))
